//
//  ReactionsView.swift
//  iOS_vMeet
//
//  Created by Gramercy on 5/12/22.
//

import UIKit

protocol ReactionsViewDelegate: AnyObject {
    func reactionTapped(type: ReactionToolbarItem.ItemType)
}

class ReactionsView: UIView {
    // MARK: Properties
    private weak var parentView: UIView?
    private weak var anchorView: UIView?
    weak var delegate: ReactionsViewDelegate?
    
    static let barColor = UIColor(white: 0.5, alpha: 1.0)
    static let iconColor = UIColor.lightGray
    private let viewHeight: CGFloat = 55.0
    private lazy var topAnchorConstraint = NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: anchorView, attribute: .top, multiplier: 1.0, constant: 0.0)
    
    // MARK: Subviews
    private let toolbar: UIToolbar = {
        let bar = UIToolbar(frame: CGRect(origin: .zero, size: CGSize(width: 200, height: 44.0)))
        bar.isTranslucent = false
        bar.barTintColor = ReactionsView.barColor
        bar.contentMode = .scaleAspectFit
        bar.translatesAutoresizingMaskIntoConstraints = false
        return bar
    }()
    
    private lazy var items: [UIBarButtonItem] = {
        var items: [UIBarButtonItem] = [flexSpace, flexSpace, flexSpace]
        ReactionToolbarItem.ItemType.allCases.forEach {
            let item = ReactionToolbarItem(type: $0, target: self, action: #selector(self.buttonTapped(sender:)))
            items.append(item)
            items.append(flexSpace)
        }
        items.append(contentsOf: [flexSpace, flexSpace])
        return items
    }()
    
    private var flexSpace: UIBarButtonItem {
        return .init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    }
    
    // MARK: Init
    required init(parentView: UIView, anchorView: UIView) {
        self.parentView = parentView
        self.anchorView = anchorView
        super.init(frame: .zero)
        
        self.setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Setup Subviews
    private func setupSubviews() {
        self.backgroundColor = ReactionsView.barColor
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(toolbar)
        NSLayoutConstraint.activate([
            toolbar.topAnchor.constraint(equalTo: self.topAnchor),
            toolbar.heightAnchor.constraint(equalToConstant: 44.0),
            toolbar.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            toolbar.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        ])
        
        toolbar.setItems(items, animated: false)
    }
    
    // MARK: Show/hide Reactions View
    public func showReactions() {
        guard let anchorView = anchorView, let parentView = parentView else { return }
        
        self.parentView?.insertSubview(self, belowSubview: anchorView)
        NSLayoutConstraint.activate([
            self.topAnchorConstraint,
            self.heightAnchor.constraint(equalToConstant: self.viewHeight),
            self.leadingAnchor.constraint(equalTo: parentView.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: parentView.trailingAnchor)
        ])
        self.parentView?.layoutSubviews()
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseOut) { [weak self] in
            self?.topAnchorConstraint.constant = -1 * (self?.viewHeight ?? 0.0)
            self?.parentView?.layoutSubviews()
        } completion: { _ in }
    }
    
    public func hideReactions(finished: @escaping () -> ()) {
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseIn) { [weak self] in
            self?.topAnchorConstraint.constant = self?.viewHeight ?? 0.0
            self?.parentView?.layoutSubviews()
        } completion: { [weak self] _ in
            self?.removeFromSuperview()
            finished()
        }
    }

    // MARK: Recursive Toolbar Image Search
    private func getFrameForToolbarItemWithImage(_ image: UIImage, viewToSearch: UIView) -> CGRect? {
        if let imageView = viewToSearch as? UIImageView {
            if imageView.image == image {
                return imageView.convert(imageView.frame, to: self.parentView)
            }
            return nil
        }
        for subView in viewToSearch.subviews {
            if let frame = getFrameForToolbarItemWithImage(image, viewToSearch: subView) {
                return frame
            }
        }
        return nil
    }
    
    // MARK: Button Tapped
    @objc private func buttonTapped(sender: Any) {
        guard let item = sender as? ReactionToolbarItem else { return }

        guard let image = item.getImageForType() else { return }
        guard let startFrame = getFrameForToolbarItemWithImage(image, viewToSearch: toolbar) else { return }
        let floatingImageView = UIImageView(frame: startFrame)
        floatingImageView.image = image
        self.parentView?.addSubview(floatingImageView)
        
        self.delegate?.reactionTapped(type: item.type)
        
        UIView.animate(withDuration: 1.2, delay: 0.0, options: .curveEaseOut) { [weak self] in
            item.tintColor = .efBlue
            floatingImageView.frame = floatingImageView.frame.offsetBy(dx: 0.0, dy: -200.0)
            floatingImageView.alpha = 0.0
            self?.parentView?.layoutSubviews()
        } completion: { _ in
            floatingImageView.removeFromSuperview()
        }
    }
}


// MARK: Reaction Toolbar Item
class ReactionToolbarItem: UIBarButtonItem {
    // Item type enum
    enum ItemType: String, CaseIterable {
        case love =    "Love"
        case like =    "Like"
        case dislike = "Dislike"
        case clap =    "Clap"
    }
    
    // Properties
    let type: ItemType
    
    // Init
    required init(type: ItemType, target: AnyObject, action: Selector) {
        self.type = type
        super.init()
        
        self.title = type.rawValue
        self.target = target
        self.action = action
        self.tintColor = ReactionsView.iconColor
        self.image = getImageForType()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Methods
    func getImageForType() -> UIImage? {
        switch self.type {
        case .love:    return UIImage(systemName: "heart.fill")
        case .like:    return UIImage(systemName: "hand.thumbsup.fill")
        case .dislike: return UIImage(systemName: "hand.thumbsdown.fill")
        case .clap:    return UIImage(systemName: "hands.clap.fill")
        }
    }
}
