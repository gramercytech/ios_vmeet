//
//  VMeet.swift
//  iOS_vMeet
//
//  Created by Gramercy on 4/22/22.
//

import UIKit

public class VMeet {
    /// VMeet asset bundle.
    static public let bundle: Bundle = {
        let bundleBase = Bundle(for: vMeetViewController.self)
        let bundlePath = bundleBase.path(forResource: "vMeetAssets", ofType: "bundle")
        if bundlePath == nil {
            print("Error- Unable to load vmeet bundle. Invalid path.")
        }
        return Bundle(path: bundlePath!)!
    }()
    
    /// Returns an initialized vMeet view controller for the given parameters.
    ///
    /// - Parameters:
    ///   - apiKey: OpenTok API key
    ///   - sessionID: ID for conencting to an open session
    ///   - token: Generated token for this user
    /// - Returns: (Optional) A vMeet view controller
    public class func initVC(apiKey: String, sessionID: String, token: String) -> vMeetViewController? {
        // Keys
        guard !apiKey.isEmpty && !sessionID.isEmpty && !token.isEmpty else {
            print("Error- Unable to load vmeet. Required keys cannot be empty."); return nil
        }
        OTC.shared.setKeys(apiKey: apiKey, sessionID: sessionID, token: token)

        // Storyboard/View Controller
        let storyboard = UIStoryboard(name: "vMeet", bundle: VMeet.bundle)
        guard let vc = storyboard.instantiateInitialViewController() as? vMeetViewController else {
            print("Error- Unable to load vmeet view controller. Storyboard initial view controller is wrong type."); return nil
        }

        return vc
    }
}
