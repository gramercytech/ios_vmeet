//
//  StreamerMaskView.swift
//  iOS_vMeet
//
//  Created by Gramercy on 4/29/22.
//

import UIKit
import SwiftyGif

/// UIView to add as a mask to vMeet user video streams. Displays relevant status info.
class StreamerMaskView: UIView {
    // MARK: Properties
    let handImage = UIImage(named: "hand.png", in: VMeet.bundle, with: nil)
    let noVideoImage = UIImage(named: "noStreamImage.png", in: VMeet.bundle, with: nil)
    let micOffImage = UIImage(named: "mic-off.png", in: VMeet.bundle, with: nil)!
    let soundwave_silent = UIImage(named: "00_Soundwave_Silent.png", in: VMeet.bundle, with: nil)!
    let soundwave_low = UIImage(assetName: "01_Soundwave_Low")!
    let soundwave_medium = UIImage(assetName: "02_Soundwave_Medium")!
    let soundwave_high = UIImage(assetName: "03_Soundwave_High")!

    
    // MARK: Subviews
    private lazy var raisedHandView: UIImageView = {
        let view = UIImageView()
        view.image = handImage
        view.tintColor = .efBlue
        view.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var cameraDisabledView: UIImageView = {
        let view = UIImageView()
        view.image = noVideoImage
        view.tintColor = .gray
        view.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var micStatusView: UIImageView = {
       let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var nameLabel: UILabel = {
       let label = UILabel()
        label.textColor = .white
        label.font = .systemFont(ofSize: 18.0, weight: .semibold)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: Init
    required init(frame: CGRect, cameraEnabled: Bool, micEnabled: Bool, soundLevel: OTCUser.SquiggleStatus, name: String, isHandRaised: Bool) {
        super.init(frame: frame)
        setupView()
        updateMask(cameraEnabled: cameraEnabled, micEnabled: micEnabled, soundLevel: soundLevel, name: name, isHandRaised: isHandRaised)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Setup View
    private func setupView() {
        self.backgroundColor = .clear
        self.layer.cornerRadius = 18.0
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(raisedHandView)
        NSLayoutConstraint.activate([
            raisedHandView.heightAnchor.constraint(equalToConstant: 36.0),
            raisedHandView.widthAnchor.constraint(equalTo: raisedHandView.heightAnchor),
            raisedHandView.topAnchor.constraint(equalTo: self.topAnchor, constant: 16.0),
            raisedHandView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16.0)
        ])
        
        self.addSubview(cameraDisabledView)
        NSLayoutConstraint.activate([
            cameraDisabledView.topAnchor.constraint(equalTo: self.topAnchor, constant: 20.0),
            cameraDisabledView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20.0),
            cameraDisabledView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20.0),
            cameraDisabledView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12.0)
        ])
        
        self.addSubview(micStatusView)
        NSLayoutConstraint.activate([
            micStatusView.heightAnchor.constraint(equalToConstant: 30.0),
            micStatusView.widthAnchor.constraint(equalToConstant: 42.0),
            micStatusView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12.0),
            micStatusView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant:  -12.0)
        ])
        
        self.addSubview(nameLabel)
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: micStatusView.topAnchor),
            nameLabel.leadingAnchor.constraint(equalTo: micStatusView.trailingAnchor, constant: 12.0),
            nameLabel.bottomAnchor.constraint(equalTo: micStatusView.bottomAnchor),
            nameLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 200.0)
        ])
    }
    
    /// Update the streamer mask to reflect their current status.
    public func updateMask(cameraEnabled: Bool, micEnabled: Bool, soundLevel: OTCUser.SquiggleStatus, name: String, isHandRaised: Bool) {
        // Camera enabled/disabled
        cameraDisabledView.isHidden = cameraEnabled
        
        // Microphone enabled/disabled
        if micEnabled {
            micStatusView.stopAnimatingGif()
            
            switch soundLevel {
            case .none:
                micStatusView.setImage(micOffImage)
                micStatusView.tintColor = .red
            case .silent:
                micStatusView.setImage(soundwave_silent)
                micStatusView.tintColor = .efBlue
            case .low:
                micStatusView.setGifImage(soundwave_low)
                micStatusView.startAnimatingGif()
            case .medium:
                micStatusView.setGifImage(soundwave_medium)
                micStatusView.startAnimatingGif()
            case .high:
                micStatusView.setGifImage(soundwave_high)
                micStatusView.startAnimatingGif()
            }
        } else {
            micStatusView.setImage(micOffImage)
            micStatusView.tintColor = .red
        }
        
        // Name label
        nameLabel.text = name
        
        // Raised hand
        raisedHandView.isHidden = !isHandRaised
        
        // Border
        switch (isHandRaised, cameraEnabled) {
        case (false, true):
            self.layer.borderColor = UIColor.clear.cgColor
        case (false, false):
            self.layer.borderColor = UIColor.gray.cgColor
            self.layer.borderWidth = 1.0
        case (true, _):
            self.layer.borderColor = UIColor.efBlue.cgColor
            self.layer.borderWidth = 6.0
        }
    }
}
