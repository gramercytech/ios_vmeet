//
//  OTC_User.swift
//  iOS_vMeet
//
//  Created by Gramercy on 4/29/22.
//

import UIKit
import OpenTok

// MARK: OTCUser
class OTCUser: NSObject, Identifiable {
    /// Status representing the user's microphone sound level.
    enum SquiggleStatus: String { case none, silent, low, medium, high }
    
    // MARK: Properties
    public weak var delegate: OTCUserDelegate?
    public var view: UIView?
    public var videoEnabled: Bool
    public var micEnabled: Bool { didSet { if micEnabled == false { micSoundLevelAverage = 0.0 } } }
    public private(set) var micSoundLevelAverage: Float = 0.0
    public private(set) var readyToUpdateAudioLevel: Bool = true
    public private(set) var micSoundLevel: SquiggleStatus = .none
    public private(set) var name: String
    public var hasRaisedHand: Bool
    
    // MARK: Init
    internal init(view: UIView?, micEnabled: Bool = true, videoEnabled: Bool = true, name: String) {
        self.view = view
        self.micEnabled = micEnabled
        self.videoEnabled = videoEnabled
        self.name = name
        self.hasRaisedHand = false
        self.delegate = OTC.shared
        super.init()
        
        startAudioUpdateTimer()
    }
    
    // MARK: Methods
    public func updateMicSoundLevel(enabled: Bool, level: Float) {
        // Check mic enabled status.
        if micEnabled != enabled {
            micEnabled = enabled
            self.delegate?.userUpdated(id: self.id)
        }
        
        // Return if mic is disabled.
        if !micEnabled { return }
        
        // Update average sound level.
        let normalizedLevel = min(level, 0.1)
        if micSoundLevelAverage == 0.0 {
            micSoundLevelAverage = normalizedLevel
        } else {
            micSoundLevelAverage = (micSoundLevelAverage * 0.5) + (normalizedLevel * 0.5)
        }
        
        // Get squiggle for new average.
        let newLevel: SquiggleStatus = {
            switch micSoundLevelAverage {
            case -1.0..<0.02: return .silent
            case 0.02..<0.05: return .low
            case 0.05..<0.08: return .medium
            default:          return .high
            }
        }()
        
        // Update delegate if needed.
        if self.micSoundLevel != newLevel {
            self.micSoundLevel = newLevel
            self.delegate?.userUpdated(id: self.id)
        }

        // Stop updating until needed.
        readyToUpdateAudioLevel = false
    }
    
    /// Schedule timer for updating average mic sound levels.
    private func startAudioUpdateTimer() {
        Timer.scheduledTimer(withTimeInterval: 0.2, repeats: true) { [weak self] timer in
            if self == nil { timer.invalidate() }
            self?.readyToUpdateAudioLevel = true
        }
    }
}


// MARK: OTCUser_Subscriber
class OTCUser_Subscriber: OTCUser, OTSubscriberKitAudioLevelDelegate {
    let user: OTSubscriber
    
    required init(user: OTSubscriber) {
        self.user = user
        let view = user.view
        let micEnabled = user.stream?.hasAudio ?? true
        let videoEnabled = user.stream?.hasVideo ?? false
        let name = "Placeholder Name"
        
        super.init(view: view, micEnabled: micEnabled, videoEnabled: videoEnabled, name: name)
        user.audioLevelDelegate = self
    }
    
    func subscriber(_ subscriber: OTSubscriberKit, audioLevelUpdated audioLevel: Float) {
        if readyToUpdateAudioLevel {
            let micEnabled = subscriber.stream?.hasAudio ?? false
            updateMicSoundLevel(enabled: micEnabled, level: audioLevel)
        }
    }
}


// MARK: OTCUser_Publisher
class OTCUser_Publisher: OTCUser, OTPublisherKitAudioLevelDelegate {
    let user: OTPublisher
    
    required init(user: OTPublisher) {
        self.user = user
        let view = user.view
        let micEnabled = user.stream?.hasAudio ?? true
        let videoEnabled = user.stream?.hasVideo ?? true
        let name = "Placeholder Name"
        
        super.init(view: view, micEnabled: micEnabled, videoEnabled: videoEnabled, name: name)
        user.audioLevelDelegate = self
    }
    
    func publisher(_ publisher: OTPublisherKit, audioLevelUpdated audioLevel: Float) {
        if readyToUpdateAudioLevel {
            updateMicSoundLevel(enabled: micEnabled, level: audioLevel)
        }
    }
}
