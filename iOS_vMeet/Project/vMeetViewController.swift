//
//  vMeetViewController.swift
//  iOS_vMeet
//
//  Created by Gramercy on 4/19/22.
//

import Foundation
import UIKit

public final class vMeetViewController: UIViewController {
    // MARK: Subviews
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var bottomToolbar: UIToolbar!
    @IBOutlet var microphoneButton: UIBarButtonItem!
    @IBOutlet var videoButton: UIBarButtonItem!
    @IBOutlet var raisedHandButton: UIBarButtonItem!
    @IBOutlet var reactionButton: UIBarButtonItem!
    @IBOutlet var moreOptionsButton: UIBarButtonItem!
    private var reactionView: ReactionsView?
    
    // MARK: Properties
    private var publisherView: UIView?
    private var subscriberViews: [UIView] = []
    private let screenDimPreviouslyDisabled: Bool = { UIApplication.shared.isIdleTimerDisabled }()
    
    // MARK: Lifecycle
    public override func viewDidLoad() {
        super.viewDidLoad()
        OTC.shared.delegate = self
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        OTC.shared.connectToSession()
    }
    
    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        // Hide the reaction view if needed.
        reactionView?.hideReactions{ [weak self] in
                self?.reactionView = nil
        }
        self.reactionButton.tintColor = .lightGray
        
        // Update collection view layout.
        if let layout = collectionView.collectionViewLayout as? vMeetLayout {
            var size = size
            if size.height >= size.width { // Moving from layout to portrait
                size.height = size.height - (self.view.safeAreaInsets.left + self.view.safeAreaInsets.right + bottomToolbar.frame.height)
            } else { // Moving from portrait to layout
                size.height = size.height - (self.additionalSafeAreaInsets.top + self.additionalSafeAreaInsets.bottom + bottomToolbar.frame.height + 20.0)
            }
            
            layout.resetLayout(newTransitionSize: size)
            collectionView.reloadData()
        }
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !screenDimPreviouslyDisabled {
            UIApplication.shared.isIdleTimerDisabled = false
        }
    }
    
    // MARK: Methods
    private func updateMaskForView(_ userView: UIView, videoEnabled: Bool, micEnabled: Bool, micSoundLevel: OTCUser.SquiggleStatus, name: String, isHandRaised: Bool) {
        if let mask = userView.subviews.first(where: { type(of: $0) == StreamerMaskView.self}) as? StreamerMaskView {
            mask.updateMask(cameraEnabled: videoEnabled, micEnabled: micEnabled, soundLevel: micSoundLevel, name: name, isHandRaised: isHandRaised)
        } else {
            let mask = StreamerMaskView(frame: view.frame, cameraEnabled: videoEnabled, micEnabled: micEnabled, soundLevel: micSoundLevel, name: name, isHandRaised: isHandRaised)
            
            userView.addSubview(mask)
            NSLayoutConstraint.activate([
                mask.topAnchor.constraint(equalTo: userView.topAnchor),
                mask.leadingAnchor.constraint(equalTo: userView.leadingAnchor),
                mask.trailingAnchor.constraint(equalTo: userView.trailingAnchor),
                mask.bottomAnchor.constraint(equalTo: userView.bottomAnchor)
            ])
        }
    }
    
    // MARK: Button Actions
    @IBAction func micButtonTapped(sender: Any) {
        OTC.shared.micStatusReversed()
    }
    
    @IBAction func videoButtonTapped(sender: Any) {
        OTC.shared.videoStatusReversed()
    }
    
    @IBAction func raisedHandButtonTapped(sender: Any) {
        OTC.shared.hasRaisedHandStatusReversed()
    }
    
    @IBAction func reactionButtonTapped(sender: Any) {
        if let reactionView = self.reactionView {
            reactionView.hideReactions { [weak self] in
                self?.reactionView = nil
            }
            self.reactionButton.tintColor = .lightGray
        } else {
            self.reactionView = .init(parentView: self.view, anchorView: self.bottomToolbar)
            self.reactionView?.delegate = self
            self.reactionView?.showReactions()
            self.reactionButton.tintColor = .efBlue
        }
    }
    
    @IBAction func moreOptionsButtonTapped(sender: Any) {
        
    }
}


// MARK: UICollectionView
extension vMeetViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subscriberViews.count + (publisherView == nil ? 0 : 1)
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "vMeetCell", for: indexPath)
        let videoView: UIView? = {
            if (indexPath.row == 0) {
                return self.publisherView
            } else {
                if subscriberViews.count >= indexPath.row {
                    return subscriberViews[indexPath.row - 1]
                } else {
                    return nil
                }
            }
        }()
        
        if let viewToAdd = videoView {
            viewToAdd.frame = cell.bounds
            cell.addSubview(viewToAdd)
        }
        
        cell.layer.cornerRadius = 18.0
        
        return cell
    }
}


// MARK: OTC Delegate
extension vMeetViewController: OTCDelegate {
    func subscriberStatusChanged(_ subscriber: OTCUser_Subscriber) {
        if let subView = subscriberViews.first(where: { $0 == subscriber.view }) {
            updateMaskForView(subView, videoEnabled: subscriber.videoEnabled, micEnabled: subscriber.micEnabled, micSoundLevel: subscriber.micSoundLevel, name: subscriber.name, isHandRaised: subscriber.hasRaisedHand)
        }
    }
    
    func publisherViewChanged(_ publisher: OTCUser_Publisher) {
        guard let pubView = publisher.view else { return }
        
        microphoneButton.image = {
            if publisher.micEnabled {
                microphoneButton.tintColor = .efBlue
                return UIImage(named: "mic-on.png", in: VMeet.bundle, with: nil)
            } else {
                microphoneButton.tintColor = .red
                return UIImage(named: "mic-off.png", in: VMeet.bundle, with: nil)
            }
        }()
        
        videoButton.image = {
            if publisher.videoEnabled {
                videoButton.tintColor = .efBlue
                return UIImage(named: "video-on.png", in: VMeet.bundle, with: nil)
            } else {
                videoButton.tintColor = .red
                return UIImage(named: "video-off.png", in: VMeet.bundle, with: nil)
            }
        }()
        
        raisedHandButton.tintColor = publisher.hasRaisedHand ? .efBlue : .lightGray
        
        self.publisherView = pubView
        updateMaskForView(pubView, videoEnabled: publisher.videoEnabled, micEnabled: publisher.micEnabled, micSoundLevel: publisher.micSoundLevel, name: publisher.name, isHandRaised: publisher.hasRaisedHand)
        self.collectionView.reloadData()
    }
    
    func subscriberListUpdated(_ subscribers: [OTCUser_Subscriber]) {
        var newViewArray: [UIView] = []
        subscribers.forEach {
            if let view = $0.view {
                newViewArray.append(view)
                if !subscriberViews.contains(view) {
                    updateMaskForView(view, videoEnabled: $0.videoEnabled, micEnabled: $0.micEnabled, micSoundLevel: $0.micSoundLevel, name: $0.name, isHandRaised: $0.hasRaisedHand)
                }
            }
        }
        self.subscriberViews = newViewArray
        self.collectionView.reloadData()
    }
}


// MARK: Reaction View Delegate
extension vMeetViewController: ReactionsViewDelegate {
    func reactionTapped(type: ReactionToolbarItem.ItemType) {
        self.reactionButtonTapped(sender: self)
    }
}
