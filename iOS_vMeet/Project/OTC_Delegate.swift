//
//  OTC_Delegate.swift
//  iOS_vMeet
//
//  Created by Gramercy on 4/29/22.
//

import UIKit

/// OpenTok Controller Delegate
protocol OTCDelegate: AnyObject {
    /// Called when the this user's (publisher) view changes.
    func publisherViewChanged(_ publisher: OTCUser_Publisher)
    /// Called when a subscriber's status updates (i.e.- mic audio levels change, or they enable/disable their camera or microphone)
    func subscriberStatusChanged(_ subscriber: OTCUser_Subscriber)
    /// Called when a subscriber stream is added/removed from the session.
    func subscriberListUpdated(_ subscribers: [OTCUser_Subscriber])

}

/// OTCUser Delegate
protocol OTCUserDelegate: AnyObject {
    /// Tells the delegate that the OTCUser object has updated itself.
    func userUpdated(id: ObjectIdentifier)
}
