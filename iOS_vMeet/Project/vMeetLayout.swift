//
//  vMeetLayout.swift
//  iOS_vMeet
//
//  Created by Gramercy on 4/18/22.
//

import UIKit

/// Custom collection view layout
class vMeetLayout: UICollectionViewLayout {
    // MARK: Properties
    fileprivate var cache = [UICollectionViewLayoutAttributes]()
    fileprivate var cachedNumberOfViews = 0
    fileprivate var contentHeight: CGFloat = 0.0
    fileprivate let insets: UIEdgeInsets = .init(top: 12.0, left: 8.0, bottom: 0.0, right: 8.0)
    fileprivate var isPortraitMode: Bool { collectionViewSize.height >= collectionViewSize.width }
    fileprivate var layoutTransitionNewSize: CGSize?
    fileprivate var collectionViewSize: CGSize {
        return layoutTransitionNewSize ?? (collectionView?.bounds.size ?? CGSize())
    }
    
    // MARK: Prepare
    override func prepare() {
        guard let views = collectionView?.numberOfItems(inSection: 0)
        else {
            cache.removeAll()
            return
        }
        
        if views != cachedNumberOfViews {
            cache.removeAll()
        }
        
        if cache.isEmpty {
            cachedNumberOfViews = views
            let attribs: [UICollectionViewLayoutAttributes] = {
                switch views {
                case 0: // No views
                    self.contentHeight = 0
                    return []
                case 1: // User only
                    return attributesForUserOnly()
                case let x: // User and others
                    if isPortraitMode {
                        return attributesForOneColumn(withNumberOfViews: x)
                    }
                    return attributesForTwoColumns(withNumberOfViews: x)
                }
            }()
            
            cache.append(contentsOf: attribs)
        }
    }
    
    // MARK: User Only Fullscreen
    fileprivate func attributesForUserOnly() -> [UICollectionViewLayoutAttributes] {
        var attribs = [UICollectionViewLayoutAttributes]()
        let ip = IndexPath(item: 0, section: 0)
        let attr = UICollectionViewLayoutAttributes(forCellWith: ip)
        let height = collectionViewSize.height - 36.0
        let width = collectionViewSize.width - 36.0
        
        if isPortraitMode {
            attr.frame = CGRect(x: +18.0, y: +18.0, width: width, height: height)
        } else {
            attr.frame = CGRect(x: +36.0, y: +18.0, width: width - 36.0, height: height)
        }
        attribs.append(attr)
        
        self.contentHeight = collectionViewSize.height
        
        return attribs
    }
    
    // MARK: One Column Of Views
    fileprivate func attributesForOneColumn(withNumberOfViews views: Int) -> [UICollectionViewLayoutAttributes] {
        var attribs = [UICollectionViewLayoutAttributes]()
        let height = (collectionViewSize.height / 2.0) - 10.0
        let width = collectionViewSize.width
        
        let userIP = IndexPath(item: 0, section: 0)
        let userAttribs = UICollectionViewLayoutAttributes(forCellWith: userIP)
        userAttribs.frame = CGRect(x: 0.0, y: 0.0, width: width, height: height).inset(by: self.insets)
        attribs.append(userAttribs)
        
        for i in (1...views) {
            let subIP = IndexPath(item: i, section: 0)
            let subAttribs = UICollectionViewLayoutAttributes(forCellWith: subIP)
            subAttribs.frame = CGRect(x: 0.0, y:(height * CGFloat(i)), width: width, height: height).inset(by: self.insets)
            attribs.append(subAttribs)
        }
        
        self.contentHeight = CGFloat(views) * height
        
        return attribs
    }
    
    // MARK: Two Columns Of Views
    fileprivate func attributesForTwoColumns(withNumberOfViews views: Int) -> [UICollectionViewLayoutAttributes] {
        var attribs = [UICollectionViewLayoutAttributes]()
        let height = collectionViewSize.height - 20.0
        let width = (collectionViewSize.width / 2.0) - 48.0
        
        attribs.append(contentsOf: attributesForViewsInRows(initialYOffset: 0,
                                                            totalNumberOfViews: views,
                                                            viewSize: CGSize(width: width, height: height),
                                                            viewOffset: 0))
        return attribs
    }
    
    // MARK: View Attributes
    fileprivate func attributesForViewsInRows(initialYOffset: CGFloat, totalNumberOfViews views: Int, viewSize: CGSize, viewOffset: Int) -> [UICollectionViewLayoutAttributes] {
        var attribs = [UICollectionViewLayoutAttributes]()
        var yOffset = initialYOffset
        
        let newLineCondition : (Int) -> Bool = {
            if viewOffset == 0 {
                return !($0 % 2 == 0)
            } else {
                return $0 % 2 == 0
            }
        }
        
        for item in viewOffset..<views {
            let ip = IndexPath(item: item, section: 0)
            let attrs = UICollectionViewLayoutAttributes(forCellWith: ip)
            let xOffset = (CGFloat(newLineCondition(item) ? viewSize.width : 0)) + 48.0
            attrs.frame = CGRect(x: xOffset, y: yOffset, width: viewSize.width, height: viewSize.height).inset(by: self.insets)
            attribs.append(attrs)
            if item > viewOffset && newLineCondition(item) {
                yOffset += viewSize.height
            }
        }
        
        self.contentHeight = yOffset + (views % 2 == 0 ? 0.0 : viewSize.height)
        
        return attribs
    }
    
    // MARK: Content Size
    override var collectionViewContentSize: CGSize {
        return CGSize(width: collectionViewSize.width, height: contentHeight)
    }
    
    // MARK: Elements
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return cache
    }
    
    // MARK: Reset Layout
    func resetLayout(newTransitionSize: CGSize) {
        self.layoutTransitionNewSize = newTransitionSize
        self.cache = []
        self.invalidateLayout()
    }
}
