//
//  ext_UIImage.swift
//  iOS_vMeet
//
//  Created by Gramercy on 5/4/22.
//

import UIKit
import SwiftyGif

extension UIImage {
    /// Loads a gif image from the vmeet NSDataAsset bundle with the given name.
    convenience init?(assetName: String) {
        do {
            if let dataAsset = NSDataAsset(name: assetName, bundle: VMeet.bundle) {
                let imgData = dataAsset.data
                try self.init(gifData: imgData)
            } else {
                print("ERROR- Unable to find data asset for name- \(assetName)")
                return nil
            }
        } catch {
            print("ERROR- Unable to load gif from asset data. returned- \(error.localizedDescription)")
            return nil
        }
    }
}
