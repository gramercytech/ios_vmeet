//
//  OpenTokController.swift
//  iOS_vMeet
//
//  Created by Gramercy on 4/18/22.
//

import UIKit
import OpenTok

/// OpenTok Controller
class OTC: NSObject {
    public static var shared = OTC()
    
    // Keys/IDs/Tokens
    private var apiKey: String?
    private var sessionID: String?
    private var token: String?
    
    // MARK: Properties
    public weak var delegate: OTCDelegate?
    private var publisher: OTCUser_Publisher?
    private var subscribers: [OTCUser_Subscriber]
    
    public private(set) lazy var session: OTSession? = {
        guard let apiKey = apiKey, let sessionID = sessionID else { return nil }
        return OTSession(apiKey: apiKey, sessionId: sessionID, delegate: self)!
    }()
    
    // MARK: Init
    private override init() {
        self.subscribers = []
        super.init()
    }
    
    // MARK: Pod Methods
    func setKeys(apiKey: String, sessionID: String, token: String) {
        self.apiKey = apiKey
        self.sessionID = sessionID
        self.token = token
    }
    
    func connectToSession() {
        guard let token = token, let session = session else { return }
        
        var error: OTError?
        session.connect(withToken: token, error: &error)
        if error != nil {
            print(error!)
        }
    }
    
    func micStatusReversed() {
        guard let pub = publisher else { return }
        pub.user.publishAudio = !pub.micEnabled
        pub.updateMicSoundLevel(enabled: !pub.micEnabled, level: pub.micSoundLevelAverage)
        self.delegate?.publisherViewChanged(pub)
    }
    
    func videoStatusReversed() {
        guard let pub = publisher else { return }
        pub.user.publishVideo = !pub.videoEnabled
        pub.videoEnabled = !pub.videoEnabled
        self.delegate?.publisherViewChanged(pub)
    }
    func hasRaisedHandStatusReversed() {
        guard let pub = publisher else { return }
        pub.hasRaisedHand = !pub.hasRaisedHand
        self.delegate?.publisherViewChanged(pub)
    }
}


// MARK: OpenTok Session Delegate
extension OTC: OTSessionDelegate {
    // Session Connected
    func sessionDidConnect(_ session: OTSession) {
        print("The client connected to the OpenTok session.")
        
        let settings = OTPublisherSettings()
        settings.name = UIDevice.current.name
        
        guard let ot_publisher = OTPublisher(delegate: self, settings: settings) else {
            print("Error- Unable to create the OTPublisher"); return
        }
        
        var error: OTError?
        session.publish(ot_publisher, error: &error)
        guard error == nil else {
            print(error!); return
        }
        
        self.publisher = OTCUser_Publisher(user: ot_publisher)
        self.delegate?.publisherViewChanged(self.publisher!)
    }
    
    // Session Disconnected
    func sessionDidDisconnect(_ session: OTSession) {
        print("The client disconnected from the OpenTok session.")
    }
    
    // Session Failed
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("The client failed to connect to the OpenTok session: \(error).")
    }
    
    // Stream Created
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("A stream was created in the session.")
        
        guard let subscriber = OTSubscriber(stream: stream, delegate: self) else {
            print("Error while subscribing"); return
        }
        
        var error: OTError?
        session.subscribe(subscriber, error: &error)
        guard error == nil else {
            print(error!); return
        }
        
        if !subscribers.contains(where: { $0.user == subscriber }) {
            let user = OTCUser_Subscriber(user: subscriber)
            self.subscribers.append(user)
            self.delegate?.subscriberListUpdated(subscribers)
        }
    }
    
    // Stream Destroyed
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("A stream was destroyed in the session.")
        if let index = subscribers.firstIndex(where: { $0.user.stream?.streamId == stream.streamId }) {
            subscribers.remove(at: index)
            self.delegate?.subscriberListUpdated(subscribers)
        }
    }
    
    // Subscriber Video Was Enabled
    func subscriberVideoEnabled(_ subscriber: OTSubscriberKit, reason: OTSubscriberVideoEventReason) {
        if let index = subscribers.firstIndex(where: { subscriber.stream == $0.user.stream }) {
            let sub = subscribers[index]
            sub.videoEnabled = true
            self.delegate?.subscriberStatusChanged(sub)
        }
    }
    
    // Subscriber Video Was Disabled
    func subscriberVideoDisabled(_ subscriber: OTSubscriberKit, reason: OTSubscriberVideoEventReason) {
        if let index = subscribers.firstIndex(where: { subscriber.stream == $0.user.stream }) {
            let sub = subscribers[index]
            sub.videoEnabled = false
            self.delegate?.subscriberStatusChanged(sub)
        }
    }
}


// MARK: OpenTok Publisher Delegate
extension OTC: OTPublisherKitDelegate {
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("The publisher failed: \(error)")
    }
}


// MARK: OpenTok Subscriber Delegate
extension OTC: OTSubscriberDelegate {
    public func subscriberDidConnect(toStream subscriber: OTSubscriberKit) {
        print("The subscriber did connect to the stream.")
    }
    
    public func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("The subscriber failed to connect to the stream.")
    }
}


// MARK: OTCUserDelegate
extension OTC: OTCUserDelegate {
    func userUpdated(id: ObjectIdentifier) {
        if let pub = self.publisher, pub.id == id {
            self.delegate?.publisherViewChanged(pub)
        }
        if let sub = subscribers.first(where: { $0.id == id }) {
            self.delegate?.subscriberStatusChanged(sub)
        }
    }
}
