//
//  ext_UIColor.swift
//  iOS_vMeet
//
//  Created by Gramercy on 5/5/22.
//

import UIKit

extension UIColor {
    /// Standard Eventfinity blue color
    class var efBlue: UIColor { UIColor(red: 0.0, green: 0.54, blue: 1.0, alpha: 1.0) }
}
