# iOS vMeet
***
This is the iOS vMeet CocoaPods project.
  

### Installation
It can be installed by entering the following into a podfile for the relevant targets-  
`
pod 'ios_vmeet', :git => 'https://git@bitbucket.org/gramercytech/ios_vmeet.git'
`
  
### Requirements
iOS min version- 11.0  
