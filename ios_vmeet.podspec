#
#  Be sure to run `pod spec lint ios_vmeet.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  # ―――  Required Specs  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  # Standard:
  spec.name             = "ios_vmeet"
  spec.version          = "1.0.20"
  spec.platform         = :ios, "13.0"
  spec.swift_versions   = ['5.0']
  spec.static_framework = true
  spec.dependency         'OpenTok'
  spec.dependency         'SwiftyGif'
  
  # Source:
  spec.source_files     = "Classes", "iOS_vMeet/**/*.{h,m,swift}"
  spec.exclude_files    = "Classes/Exclude"
  spec.source           = { :git => "https://git@bitbucket.org/gramercytech/ios_vmeet.git", :tag => "#{spec.version}" }
  spec.resource_bundles = {
    'vMeetAssets' => ['iOS_vMeet/Project/**/*.{json,xcassets,json,png,svg,gif,docc,md,imageset,dataset,storyboard}']
  }
  
  # Meta
  spec.homepage         = "https://bitbucket.org/gramercytech/ios_vmeet/src/main/"
  spec.author           = { "Philip Ross" => "philip.ross@gramercytech.com" }
  spec.summary          = "This is the iOS vMeet CocoaPods project. It integrates vMeet into iOS."
  spec.description      = <<-DESC
                          vMeet is a sizable project and needs to be created as it's own standalone cocoapod.
                          DESC
  spec.license          = { :type => 'Gramercy Tech, Version 0.1', :text =>
                          <<-LICENSE
                          ABSOLUTELY NO PERMISSIONS ARE GRANTED TO VIEW/USE/MODIFY/REPRODUCE/ETC ANY PORTION OF THIS PROJECT. THIS IS A PRIVATE INTERNAL PROJECT WHICH IS OWNED BY GRAMERCY TECH.
                          LICENSE
                          }
  

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #
  

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  
  # spec.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See https://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  # spec.license      = { :type => "MIT", :file => "FILE_LICENSE" }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #


  # Or just: spec.author    = "Philip Ross"
  # spec.authors            = { "Philip Ross" => "philip.ross@gramercytech.com" }
  # spec.social_media_url   = "https://twitter.com/Philip Ross"

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this Pod runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

  #  When using multiple platforms
  # spec.ios.deployment_target = "13.0"
  # spec.osx.deployment_target = "10.7"
  # spec.watchos.deployment_target = "2.0"
  # spec.tvos.deployment_target = "9.0"


  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #


  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  CocoaPods is smart about how it includes source code. For source files
  #  giving a folder will include any swift, h, m, mm, c & cpp files.
  #  For header files it will include any header in the folder.
  #  Not including the public_header_files will make all headers public.
  #

  # spec.public_header_files = "Classes/**/*.h"


  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # spec.resource  = "icon.png"
  # spec.resources = "Resources/*.png"

  # spec.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # spec.framework  = "SomeFramework"
  # spec.frameworks = "SomeFramework", "AnotherFramework"

  # spec.library   = "iconv"
  # spec.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # spec.requires_arc = true
  # spec.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }

end
